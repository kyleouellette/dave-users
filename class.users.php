<?php

class User extends Database{

   function __construct(){
      parent::__construct('127.0.0.1', 'root', '', 'Book');
   }

   function exists($email){
      return (boolean) count($this->get('select id from users where email=:email', array(':email' => $email)));
   }

   function register($fname, $lname, $email){
      if(  $this->exists($email) ){
         return false;
      }

      $res = $this->insert('insert into users set fname=:fname, lname=:lname, email=:email', array(
         'fname' => $fname,
         'lname' => $lname,
         'email' => $email
      ));

      return $res;
   }
}