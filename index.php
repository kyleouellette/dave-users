<?php
require('class.database.php');
require('class.users.php');

$user = new User();
$allow_form = true;

/**
 * for the sake of this example,
 * if you upload an image, we will save the image and redirect
 * you to display the file. We will NOT save user information at this time.
 */
if( isset($_FILES['image']) && !empty($_FILES['image']['filename'])){
   $file = fopen($_FILES['image']['tmp_name'], 'r');
   $image_content = fread($file, $_FILES['image']['size']);
   fclose($file);

   $image_content = addslashes($image_content);
   $res = $user->insert('insert into files set content=:content, content_type=:type', array(
      'content' => $image_content,
      'type' => $_FILES['image']['type']
   ));

   header('location: file.php');
}

?>
<!DOCTYPE html>
<html>
<head>
   <title>User reg</title>
   <link rel="stylesheet" type="text/css" href="assets/_css/main.css">
</head>
<body>
<?php
if(isset($_POST['email'])){

   if( $user->exists($_POST['email']) ){
      echo '<p>Sorry, that user is registered</p>';
   }else{
      if( $user->register($_POST['fname'], $_POST['lname'], $_POST['email']) ){
         echo '<p>The username is yours!</p>';
         $allow_form = false;
      }else{
         echo '<p>seems to have failed to create</p>';
      }
   }
}

?>
<?php if($allow_form): ?>
<form method="post" enctype="multipart/form-data">
   <label for="fname">First Name</label>
   <input type="text" name="fname" id="fname">
   <label for="lname">Last Name</label>
   <input type="text" name="lname" id="lname">
   <label for="email">Email</label>
   <input type="email" name="email" id="email">
   <label>File: <input type="file" name="image" /></label>
   <button class="button" type="submit">Create!</button>
</form>
<?php endif; ?>

</body>
</html>