<?php

class Database{
   public static $db;

   function __construct($host, $uname, $pass, $dbname){
      $this->db = new PDO("mysql:host=$host;dbname=$dbname", $uname, $pass);
      $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
      return $this;
   }

   function get($query, $args = array() ){
      $stmt = $this->db->prepare($query);
      $res = $stmt->execute($args);
      return $stmt->fetchAll();
   }

   function insert($query, $args = array()){
      $stmt = $this->db->prepare($query);
      return $stmt->execute($args);
   }
}